<h1>Get set up:</h1>
<ul>
<li>This project requires Alias to work, please download and run [this file](https://gitlab.com/roconnor/AliasCreator/raw/master/releases/V1/alias.exe), then run the command<code>alias install all</code></li>
<li>Clone this project to anywhere on your C drive.</li>
<li>Run the setup.bat</li>
<li>Input your password when prompted</li>
</ul>
You're now set up! This utility backs up at 12.00 every day, but only if you're logged on. If you wish to change this, 
I'm afraid you have to go into the scheduler yourself and edit it (a future release will allow for custom timing and directories
and all sorts of good stuff).