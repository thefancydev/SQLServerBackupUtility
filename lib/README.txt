Created by Rhys O'Connor 2015/2016

This utility backs your SQL databases up at 12.00 every day.

Please note:
-Only works if logged on
-Only works for .\SQLEXPRESS
-uses the %MACHINENAME%~%SERVERNAME~-%DATE% layout
-makes a folder called SQL in your C:\ drive. This is where the files and backups live.
-deletes backups older than 2 weeks. If you wish to change this please see below

Changing the deletion time:
-Go into backupCommand.bat (right click it, then go to edit)
-You'll see a line at the bottom saying FORFILES /S /D -14 /C "cmd /c IF @isdir == TRUE rm -rf @path"
-change the -14 to the amount of days you want to keep backups for.
NOTE: KEEP THE MINUS SIGN!

The folder that has just opened is where the databases will be backed up to.

I suggest you run backupCommand if it's past 12.00, as you might want a backup for today. Also you can get a feel for how the utility works.