DECLARE @BackupLocation AS NVARCHAR(255) = 'C:\SQL\backups\'


DECLARE @ServerFinal AS NVARCHAR(50) = REPLACE(@@SERVERNAME, '\', '~')


DECLARE Databases CURSOR FOR
SELECT name
FROM sys.databases
WHERE name
NOT IN
('tempdb',
'master',
'model',
'msdb') --put any databases here that you don't want to back up. These are here as they can't be backed up

OPEN Databases

DECLARE @DatabaseName AS NVARCHAR(255)
DECLARE @BackupDir NVARCHAR(255)
DECLARE @Command AS NVARCHAR(MAX)
DECLARE @CurrentDate AS NVARCHAR(MAX) = CONVERT(char(10), GetDate(),126)

FETCH NEXT FROM Databases
INTO @DatabaseName
PRINT @DatabaseName
 
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @BackupDir = '''' + @BackupLocation + @ServerFinal + '-' + @CurrentDate + '\' + @DatabaseName + '.BAK' + ''''
    SET @Command = 'BACKUP DATABASE ' + '[' + @DatabaseName + ']' + ' TO DISK = ' + @BackupDir
	PRINT @Command
	EXEC (@Command)
    FETCH NEXT FROM Databases
    INTO @DatabaseName
END
 
CLOSE Databases
DEALLOCATE Databases