@echo off

if not exist C:\Users\%username%\AppData\Roaming\aliases (echo "required utility (Alias) is not installed, please download and install from 'www.gitlab.com/roconnor/AliasCreator'" && pause) ELSE (goto Installed)

start https://gitlab.com/roconnor/AliasCreator

goto:eof


:Installed

alias install rerdir

set server=%ComputerName%\SQLEXPRESS

REM has to use machine name due to .\ being converted by SQLCMD instance

set replacedServer=%ComputerName%~SQLEXPRESS

for /f "tokens=2 delims==" %%G in ('wmic os get localdatetime /value') do set datetime=%%G

set year=%datetime:~0,4%
set month=%datetime:~4,2%
set day=%datetime:~6,2%

set replacedDate=%year%-%month%-%day%

mkdir %replacedserver%-%replacedDate%

sqlcmd -S %server% -i backupQuery.sql

FORFILES /S /D -14 /C "cmd /c IF @isdir == TRUE rerdir @path"
REM deletes folders older than 2 weeks (14 days). Uses rm from cygwin as rmdir only deletes folders