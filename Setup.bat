@echo off
set copyDir=%cd%\lib

c:\
cd \
mkdir SQL
cd SQL
mkdir backups
cd backups
copy %copyDir%\backupCommand.bat %cd%\backupCommand.bat
copy %copyDir%\backupQuery.sql %cd%\backupQuery.sql
copy %copyDir%\README.txt %cd%\README.txt

SchTasks /V1 /Create /SC DAILY /TN "SQLBACKUP" /TR "C:\SQL\backups\backupCommand.bat" /ST 12:00

start %cd%